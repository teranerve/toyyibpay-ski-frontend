export default {
  processItem(animal) {
    const countryAnimal = [...this.korbanMalaysia, ...this.korbanLuarNegara]
    let arrPart = []
    let animalObj = {}
    let animalArr = null
    let filteredData = null

    if (animal === 'Lembu') animalArr = this.lembuArr.data
    else if (animal === 'Unta') animalArr = this.untaArr.data
    else if (animal === 'Kambing') animalArr = this.kambingArr.data

    // Try Catch error
    try {
      // Loop to check all negeri
      countryAnimal.forEach((val) => {
        filteredData = animalArr.filter((val2) => {
          return val2.NEGARA === val.implementerNegara
        })

        // Loop to check all data
        filteredData.forEach((val2) => {
          arrPart.push({
            id: val2.configurationId,
            price: val2.HARGA,
            category: val.implementerKawasan,
            category2: val.implementerNegara,
            varient: val2.configurationParentId === '2' ? 'Ekor' : 'Bahagian',
          })
        })
      })
      animalObj = {
        animal: animal,
        arrPart,
      }
    } catch (error) {
      return false
    }

    return animalObj
  },
  getCart() {
    if (this.tempahans.length === 0) return false
    let cart = []
    try {
      for (let i = 0; i < this.tempahans.length; i++) {
        let id = this.tempahans[i].livestockid
        let animal = this.tempahans[i].jenis.split(' ')[0]
        let quantity = this.tempahans[i].jenis.split(' ')[1]
        let category = this.tempahans[i].jenis.split(' ')[2]
        let price = this.tempahans[i].price

        cart.push({
          item_id: id,
          item_name: animal,
          affiliation: this.$route.params.namaEjen
            ? this.$route.params.namaEjen
            : 'yayasanikhlas',
          currency: 'MYR',
          price: parseFloat(price),
          item_variant: category,
          quantity: parseInt(quantity),
          item_category: this.tempahans[i].kawasan,
          item_category2: this.tempahans[i].negara,
        })
      }
    } catch (error) {
      return []
    }
    return cart
  },
  getCartPembayaran() {
    if (this.tempahanDetailsInfo.length === 0) return false
    let cart = []
    try {
      for (let i = 0; i < this.tempahanDetailsInfo.length; i++) {
        let animal = this.tempahanDetailsInfo[i].tempahanDetailsHaiwanType
        let quantity = this.tempahanDetailsInfo[i].tempahanDetailsBhgnCount
        let category = this.tempahanDetailsInfo[i].tempahanDetailsBhgnType

        cart.push({
          item_name: animal,
          affiliation: this.$route.params.namaEjen
            ? this.$route.params.namaEjen
            : 'yayasanikhlas',
          currency: 'MYR',
          price: parseFloat(this.tempahanDetailsInfo[i].tempahanDetailsPrice),
          item_variant: category,
          quantity: parseInt(quantity),
          item_category: this.tempahanDetailsInfo[i].implementerKawasan,
          item_category2: this.tempahanDetailsInfo[i].implementerNegara,
        })
      }
    } catch (error) {
      return []
    }
    return cart
  },
  getCartPixel() {
    // console.log(this.tempahans)
    if (this.tempahans.length === 0) return false
    let cart = []
    try {
      for (let i = 0; i < this.tempahans.length; i++) {
        let id = this.tempahans[i].livestockid
        let animal = this.tempahans[i].jenis.split(' ')[0]
        let quantity = this.tempahans[i].jenis.split(' ')[1]
        let category = this.tempahans[i].jenis.split(' ')[2]
        let price = this.tempahans[i].price

        cart.push({
          id: id,
          quantity: parseInt(quantity),
          name: animal,
          category: category,
          value: parseFloat(price),
        })
      }
    } catch (error) {
      return []
    }
    return cart
  },
  getCartUA() {
    // console.log(this.tempahans)
    if (this.tempahans.length === 0) return false
    let cart = []
    try {
      for (let i = 0; i < this.tempahans.length; i++) {
        let id = this.tempahans[i].livestockid
        let animal = this.tempahans[i].jenis.split(' ')[0]
        let quantity = this.tempahans[i].jenis.split(' ')[1]
        let category = this.tempahans[i].jenis.split(' ')[2]
        let price = this.tempahans[i].price

        cart.push({
          id: id,
          quantity: parseInt(quantity),
          name: animal,
          category: category,
          price: parseFloat(price),
        })
      }
    } catch (error) {
      return []
    }
    return cart
  },
  GTMViewItemList(animal) {
    // Process animal item list
    let item = this.processItem(animal)
    // console.log(item)
    if (item) {
      let itemArr = []
      let itemID = []
      let UAitemArr = []
      item.arrPart.forEach((val) => {
        itemArr.push({
          item_id: val.id,
          affiliation: this.$route.params.namaEjen
            ? this.$route.params.namaEjen
            : 'yayasanikhlas',
          currency: 'MYR',
          price: val.price,
          quantity: 1,
          item_category: val.category,
          item_category2: val.category2,
          item_variant: val.varient,
        })

        itemID.push(val.id)

        UAitemArr.push({
          id: val.id,
          price: val.price,
          category: val.category2,
          variant: val.varient,
        })
      })

      if (this.$gtm.enabled()) {
        try {
          // Universal Analytics - productClicks
          this.$gtm.trackEvent({
            event: 'ua_product_click',
            ecommerce: {
              actionField: { list: item.animal },
              products: UAitemArr,
            },
          })

          // Google Analytics 4 - view_item_list
          this.$gtm.trackEvent({
            event: 'view_item_list',
            item_list_name: item.animal,
            items: itemArr,
          })

          // Google Analytics 4 - view_item
          this.$gtm.trackEvent({
            event: 'view_item',
            value:
              item.animal === 'Lembu'
                ? 390
                : item.animal === 'Kambing'
                ? 800
                : item.animal === 'Unta'
                ? 580
                : 0,
            currency: 'MYR',
            items: [
              {
                item_name: item.animal,
                quantity: 1,
              },
            ],
          })

          // Meta Pixel - view_content
          this.$gtm.trackEvent({
            event: 'pixel_view_content',
            content_name: 'Pemilihan Haiwan',
            content_type: 'product',
            content_ids: itemID,
          })
        } catch (error) {
          
        }
      }
    }
  },
  GTMSelectItem(country, animal) {
    if (!country || !animal) return false
    console.log('this.korbanMalaysia', this.korbanMalaysia)
    if (this.$gtm.enabled()) {
      try {
        // Google Analytics 4 - select_item
        this.$gtm.trackEvent({
          event: 'select_item',
          item_list_name: animal,
          items: [
            {
              item_name: country,
              affiliation: this.$route.params.namaEjen
                ? this.$route.params.namaEjen
                : 'yayasanikhlas',
            },
          ],
        })
      } catch (error) {
        
      }
    }
  },
  GTMAddToCart(index, option, country, animal) {
    if (!option || !country || !animal) return false
    let cart = this.getCart()
    let cartUA = this.getCartUA()
    let cartPixel = this.getCartPixel()

    if (cart && cart.length > 0) {
      if (this.$gtm.enabled()) {
        try {
          // Universal Analytics - AddtoCart
          this.$gtm.trackEvent({
            event: 'ua_add_to_cart',
            ecommerce: {
              add: {
                products: cartUA,
              },
            },
          })

          // Google Analytics 4 Event - add_to_cart
          this.$gtm.trackEvent({
            event: 'add_to_cart',
            currency: 'MYR',
            value: this.checkoutPrice ? this.checkoutPrice : 0,
            items: cart,
          })

          // Meta Pixel Event - AddToCart
          this.$gtm.trackEvent({
            event: 'pixel_add_to_cart',
            currency: 'MYR',
            content_type: 'product',
            contents: cartPixel,
            value: this.checkoutPrice ? this.checkoutPrice : 0,
          })
        } catch (error) {
          
        }
      }
    }
  },
  GTMRemoveFromCart(index) {
    if (this.tempahans[index].option && this.tempahans[index].price) {
      if (this.$gtm.enabled()) {
        try {
          let animal = this.tempahans[index].jenis.split(' ')[0]
          let quantity = this.tempahans[index].jenis.split(' ')[1]
          let category = this.tempahans[index].jenis.split(' ')[2]

          // Universal Analytics - removeFromCart
          this.$gtm.trackEvent({
            event: 'ua_remove_from_cart',
            ecommerce: {
              remove: {
                products: [
                  {
                    name: animal,
                    price: parseFloat(this.tempahans[index].price),
                    variant: category,
                    quantity: parseInt(quantity),
                  },
                ],
              },
            },
          })

          // Google Analytics 4 Event - remove_from_cart
          this.$gtm.trackEvent({
            event: 'remove_from_cart',
            currency: 'MYR',
            value: parseFloat(this.tempahans[index].price),
            items: [
              {
                item_name: animal,
                affiliation: this.$route.params.namaEjen
                  ? this.$route.params.namaEjen
                  : 'yayasanikhlas',
                currency: 'MYR',
                price: parseFloat(this.tempahans[index].price),
                item_variant: category,
                quantity: parseInt(quantity),
              },
            ],
          })
        } catch (error) {
          
        }
      }
    }
  },
  GTMRemoveFromCartPayment(index) {
    if (this.tempahanDetailsInfo && this.tempahanDetailsInfo.length > 0) {
      if (this.$gtm.enabled()) {
        try {
          let animal = this.tempahanDetailsInfo[index].tempahanDetailsHaiwanType
          let quantity = this.tempahanDetailsInfo[index]
            .tempahanDetailsBhgnCount
          let category = this.tempahanDetailsInfo[index].tempahanDetailsBhgnType

          this.$gtm.trackEvent({
            event: 'remove_from_cart',
            currency: 'MYR',
            value: parseFloat(
              this.tempahanDetailsInfo[index].tempahanDetailsPrice
            ),
            items: [
              {
                item_name: animal,
                affiliation: this.$route.params.namaEjen
                  ? this.$route.params.namaEjen
                  : 'yayasanikhlas',
                currency: 'MYR',
                price: parseFloat(
                  this.tempahanDetailsInfo[index].tempahanDetailsPrice
                ),
                item_variant: category,
                quantity: parseInt(quantity),
              },
            ],
          })
        } catch (error) {
          
        }
      }
    }
  },
  GTMViewCart() {
    let cart = this.getCart()
    if (cart && cart.length > 0) {
      if (this.$gtm.enabled()) {
        try {
          this.$gtm.trackEvent({
            event: 'view_cart',
            currency: 'MYR',
            value: this.checkoutPrice ? this.checkoutPrice : 0,
            items: cart,
          })
        } catch (error) {
          
        }
      }
    }
  },
  GTMBeginCheckout() {
    let cart = this.getCart()
    let cartUA = this.getCartUA()
    if (cart && cart.length > 0) {
      if (this.$gtm.enabled()) {
        try {
          // Universal Analytics - checkout
          this.$gtm.trackEvent({
            event: 'ua_checkout',
            ecommerce: {
              checkout: {
                actionField: { step: 1, option: 'Online Banking' },
                products: cartUA,
              },
            },
          })

          // Google Analytics 4 Event - begin_checkout
          this.$gtm.trackEvent({
            event: 'begin_checkout',
            currency: 'MYR',
            value: this.checkoutPrice ? this.checkoutPrice : 0,
            items: cart,
          })

          // set localstorage for checkout false
          window.localStorage.setItem('receipt', false)
        } catch (error) {
          
        }
      }
    }
  },
  GTMAddPaymentInfo() {
    let cart = this.getCartPembayaran()
    if (cart && cart.length > 0) {
      if (this.$gtm.enabled()) {
        try {
          this.$gtm.trackEvent({
            event: 'add_payment_info',
            currency: 'MYR',
            value: this.checkoutPrice ? this.checkoutPrice : 0,
            payment_type: 'Online Banking',
            items: cart,
          })
        } catch (error) {
          
        }
      }
    }
  },
  GTMPurchase(detail, status, orderID, paidAmount, chargeFee) {
    if ((status === 1 || status === '1') && paidAmount > 0) {
      if (
        window.localStorage.getItem('receipt') === 'false' ||
        window.localStorage.getItem('receipt') === null
      ) {
        let itemArr = []
        let itemUA = []
        let itemContent = []
        for (let i = 0; i < detail.length; i++) {
          itemArr.push({
            item_id: detail[i].liveStokNo,
            item_name: detail[i].tempahanDetailsHaiwanType,
            price: parseFloat(detail[i].tempahanDetailsPrice),
            item_category: detail[i].implementerKawasan,
            item_category2: detail[i].implementerNegara,
            item_variant: detail[i].tempahanDetailsBhgnType,
            quantity: 1,
            currency: 'MYR',
            // affiliation: this.$route.params.namaEjen ? this.$route.params.namaEjen : 'yayasanikhlas'
          })

          itemContent.push({
            id: detail[i].liveStokNo,
            quantity: 1,
            price: parseFloat(detail[i].tempahanDetailsPrice),
            currency: 'MYR',
          })

          itemUA.push({
            id: detail[i].liveStokNo,
            name: detail[i].tempahanDetailsHaiwanType,
            price: parseFloat(detail[i].tempahanDetailsPrice),
            category: detail[i].implementerNegara,
            variant: detail[i].tempahanDetailsBhgnType,
            quantity: 1,
          })
        }

        if (this.$gtm.enabled()) {
          try {
            // Universal Analytics - purchase
            this.$gtm.trackEvent({
              event: 'ua_purchase',
              ecommerce: {
                purchase: {
                  actionField: {
                    id: orderID || 'NULL',
                    revenue: paidAmount || 0,
                    tax: chargeFee || 0,
                  },
                  products: itemUA,
                },
              },
            })

            // Google Analytics 4 Event - purchase
            this.$gtm.trackEvent({
              event: 'purchase',
              transaction_id: orderID || 'NULL',
              // affiliation: this.checkoutPrice ? this.checkoutPrice : 0,
              value: paidAmount || 0,
              tax: chargeFee || 0,
              currency: 'MYR',
              items: itemArr,
            })

            // Meta Pixel Event - purchase
            this.$gtm.trackEvent({
              event: 'pixel_purchase',
              value: paidAmount || 0,
              currency: 'MYR',
              content_type: 'product',
              contents: itemContent,
            })

            // set localstorage for checkout false
            window.localStorage.setItem('receipt', true)
          } catch (error) {
            window.localStorage.setItem('receipt', true)
            
          }
        }
      }
    }
  },
  GTMLead() {
    // pixel_lead
    if (this.$gtm.enabled()) {
      try {
        this.$gtm.trackEvent({
          event: 'pixel_lead',
          content_name: 'Pendaftaran',
        })
      } catch (error) {
        
      }
    }
  },
}
